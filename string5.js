// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.

function convertToString(str) {
  
  if (!str.length) {
    return "";
  }
  str = str.join(" ");
  str+='.'

  return str;
}

module.exports = convertToString;
