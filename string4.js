// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

function getFullName(nameObj) {
  if (!nameObj) {
    return "";
  }

  let nam = "";
  for (let i in nameObj) {
    nam +=
      nameObj[i].charAt(0).toUpperCase() +
      nameObj[i].slice(1).toLowerCase() +
      " ";
  }

  return nam;
}

module.exports = getFullName;
