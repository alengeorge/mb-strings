// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].
// Support IPV4 addresses only. If there are other characters detected, return an empty array.

function listOutIp(ip) {
  ip = ip
    .split(".")
    .map((a) => Number(a))
    .filter((a) => {
      if (a < 256 && a > -1) {
        return true;
      }
    });

    if (ip.length != 4) {
        return []
      } 
  return ip;
}

module.exports = listOutIp;
