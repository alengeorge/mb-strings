// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.

function getMonthfromDate(date) {
  let month = parseInt(date.split("/")[1]);
  //   console.log(month);
  switch (month) {
    case 1:
      return "January";
    case 2:
      return "February";
    case 3:
      return "March";
    case 4:
      return "April";
    case 5:
      return "May";
    case 6:
      return "June";
    case 7:
      return "July";
    case 8:
      return "August";
    case 9:
      return "September";
    case 10:
      return "October";
    case 11:
      return "November";
    case 12:
      return "December";
    default:
      return "Invalid Input";
  }
}

module.exports = getMonthfromDate;
